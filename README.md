# `labotekh.labotekh.ws_restore`
The `labotekh.labotekh.ws_restore` role handle the restoration of data for each service.

## How to create backup configuration

The `backup_restore.sh` file placed in the configuration folder of the service will be executed to restore the service's database from the `files` folder. Example of restore script with a Django project :
```sh
# backup_restore.sh
#!/bin/sh
docker-compose exec -u runner web python3 manage.py migrate admin
docker-compose exec -u runner web python3 manage.py loaddata files/backup.json
```

## Tips

Prefix the files generated and used by your scripts with `backup` so they can be deleted by cleaning tasks.
